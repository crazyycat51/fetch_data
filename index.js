// // // function makeRequest(url) {
// // //     var httpRequest = false;

// // //     if (window.XMLHttpRequest) { // Mozilla, Safari, ...
// // //         httpRequest = new XMLHttpRequest();
// // //         if (httpRequest.overrideMimeType) {
// // //             httpRequest.overrideMimeType('text/xml');
            
// // //         }
// // //     } else if (window.ActiveXObject) { // IE
// // //         try {
// // //             httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
// // //         } catch (e) {
// // //             try {
// // //                 httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
// // //             } catch (e) {}
// // //         }
// // //     }

// // //     if (!httpRequest) {
// // //         alert('Невозможно создать экземпляр класса XMLHTTP ');
// // //         return false;
// // //     }
// // //     httpRequest.onreadystatechange = function() { alertContents(httpRequest); };
// // //     httpRequest.open('GET', url, true);
// // //     httpRequest.send(null);

// // // }

// // // function alertContents(httpRequest) {

// // //     if (httpRequest.readyState == 4) {
// // //         if (httpRequest.status == 200) {
// // //             var xmldoc = httpRequest.responseXML;
// // //             var root_node = xmldoc.getElementsByTagName('root').item(0);
// // //             alert(root_node.firstChild.data);
// // //         } else {
// // //             alert('С запросом возникла проблема.');
// // //         }
// // //     }

// // // }


// // // httpRequest = new XMLHttpRequest();


// // // httpRequest.open('GET', 'https://ajax.test-danit.com/api/swapi/starships/', true);
// // // httpRequest.send(null);

// // const dataContainer = document.getElementById('filmDataContainer');

// // function sendRequest(url) {
// //   return fetch(url)
// //     .then(response => {
// //       return response.json()
// //     })
// //     .catch(e => {
// //       console.error(`Something is bad ---> ${e}`)
// //     });
// // }

// // sendRequest('https://ajax.test-danit.com/api/swapi/films')
// //   .then(({results}) => {
// //     results
// //       .map(({opening_crawl, episodeId, characters}) => {
// //         const character = Promise
// //           .all(characters.map(url => sendRequest(url)))
// //           .then(charactersList => {
// //             charactersList.forEach(({name}) => {
// //               console.log(name);
// //             });
// //             const ulFilmDataList = document.createElement('ul');
// //             dataContainer.appendChild(ulFilmDataList);
// //             ulFilmDataList.innerHTML =
// //             //   `<li>Film: ${title}
// //                  `<ul>
// //                    <li>Episode: ${episodeId}</li>
// //                    <li>Description: ${opening_crawl}</li>
// //                    <li>Character name:
// //                      ${charactersList.join('<br/>')}
// //                    </li>
// //                  </ul>`
// //             //    </li>`;
// //           })
// //       })
// //   })
// //   .catch((e) => {
// //         console.error(`Error ---> ${e}`)
// //   })

// // let url = 'https://ajax.test-danit.com/api/swapi/films';
// // let response =  fetch(url);

// // if (response.ok) { // якщо HTTP-статус у діапазоні 200-299
// //   // отримуємо тіло відповіді (див. цей метод нижче)
// //   let json = response.json();
// // } else {
// //   alert("Помилка HTTP:" + response.status);
// // }


// // let url = 'https://ajax.test-danit.com/api/swapi/films';
// // let response = fetch(url);

// // let commits = response.json; 

// // alert(commits);


// let url = 'https://ajax.test-danit.com/api/swapi/films';

// let response = await fetch(url);

// if (response.ok) { // якщо HTTP-статус у діапазоні 200-299
//   // отримуємо тіло відповіді (див. цей метод нижче)
//   let json = await response.json();
// } else {
//   alert("Помилка HTTP:" + response.status);
// }




function renderMovie(movie) {
  console.log(movie);
  const div = document.createElement('div');
  const movieName = document.createElement('span');
  movieName.innerText = movie.name;
  const chars = document.createElement('div');
  div.append(movieName);
  div.append(chars);

  document.body.append(div);

  const loader = document.createElement('div');
  loader.innerText = 'LOADING!!!!!!!!!!!!!!!!!!!!!!';
  div.append(loader);

  Promise.all(movie.characters.map(charUrl => fetch(charUrl).then(r => r.json()))).then(characters => { 
    console.log('1', characters)
    loader.innerText = ' ';
    characters.forEach(character => {
      const c = document.createElement('span');
      c.innerText = character.name;
      chars.append(c);
    })
  })
}

fetch('https://ajax.test-danit.com/api/swapi/films').then(r => r.json()).then(movies => {
  movies.forEach(movie =>renderMovie(movie));
})